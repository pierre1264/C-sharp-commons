﻿using System;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        private const String MissingFilesDir1 = "Missing1.txt";
        private const String MissingFilesDir2 = "Missing2.txt";
        private const String NonMatching = "Different.txt";
        private const String ComparedFiles = "ComparedFiles.txt";
        private static int fileCount = 0;

        static void Main(string[] args)
        {
            if(CheckArguments(args))
            {
                if(File.Exists(args[0]))
                {
                    if(CompareFile(args[0], args[1]))
                    {
                        Console.WriteLine("Files are identical.");
                    }
                    else
                    {
                        Console.WriteLine("Files dot not match.");
                    }
                }
                else if(Directory.Exists(args[0]))
                {
                    CompareDirectories(args[0], args[1]);
                }

            }

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        private static bool CompareFile(String file1, String file2)
        {
            if (CompareFileSizes(file1, file2) && CompareFileBytes(file1, file2))
            {
                return true;
            }

            return false;
        }

        private static void ClearResultFiles()
        {
            File.Delete(MissingFilesDir1);
            File.Delete(MissingFilesDir2);
            File.Delete(NonMatching);
            File.Delete(ComparedFiles);
        }

        private static bool CheckArguments(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("2 arguments required");
                return false;
            }

            if (!File.Exists(args[0])
                && !Directory.Exists(args[0]))
            {
                Console.WriteLine("First argument is not a file nor a directory");
                return false;
            }

            if (!File.Exists(args[1])
                && !Directory.Exists(args[1]))
            {
                Console.WriteLine("Second argument is not a file nor a directory");
                return false;
            }

            if ((File.Exists(args[0]) && Directory.Exists(args[1]))
                || (File.Exists(args[1]) && Directory.Exists(args[0])))
            {
                Console.WriteLine("Both arguments must be files or both argulements must be directories");
                return false;
            }

            return true;
        }

        private static void CompareDirectories(String path1, String path2)
        {
            DirectoryInfo dir1 = new DirectoryInfo(path1);
            DirectoryInfo dir2 = new DirectoryInfo(path2);

            foreach(FileInfo file1 in dir1.GetFiles())
            {
                String file2 = dir2.FullName + @"\" + file1.Name;

                Console.Write("\r" + fileCount + ":" + file2 + "                 ");
                File.AppendAllText(ComparedFiles, file2 + "\n");

                fileCount++;

                if (File.Exists(file2))
                {
                    if(!CompareFile(file1.FullName, file2))
                    {
                        File.AppendAllText(NonMatching, file2 + "\n");
                    }
                }
                else
                {
                    File.AppendAllText(MissingFilesDir2, file2 + "\n");
                }
            }

            foreach (FileInfo file2 in dir2.GetFiles())
            {
                String file1 = dir1.FullName + @"\" + file2.Name;
                if(!File.Exists(file1))
                {
                    File.AppendAllText(MissingFilesDir1, file1 + "\n");
                }
            }

            foreach (DirectoryInfo subdir1 in dir1.GetDirectories())
            {
                String subdir2 = dir2.FullName + @"\" + subdir1.Name;

                if (Directory.Exists(subdir2))
                {
                    CompareDirectories(subdir1.FullName, subdir2);
                }
                else
                {
                    File.AppendAllText(MissingFilesDir2, subdir2 + "\n");
                }
            }

            foreach (DirectoryInfo subdir2 in dir2.GetDirectories())
            {
                String subdir1 = dir1.FullName + @"\" + subdir2.Name;

                if (!Directory.Exists(subdir1))
                {
                    File.AppendAllText(MissingFilesDir1, subdir1 + "\n");
                }
            }
        }

        private static bool CompareFileSizes(string fileName1, string fileName2)
        {
            
            bool fileSizeEqual = true;

            // Create System.IO.FileInfo objects for both files
            FileInfo fileInfo1 = new FileInfo(fileName1);
            FileInfo fileInfo2 = new FileInfo(fileName2);

            // Compare file sizes
            if (fileInfo1.Length != fileInfo2.Length)
            {
                // File sizes are not equal therefore files are not identical
                fileSizeEqual = false;
            }

            return fileSizeEqual;
        }

        private static bool CompareFileBytes(string fileName1, string fileName2)
        {
            // Compare file sizes before continuing. 
            // If sizes are equal then compare bytes.
            if (CompareFileSizes(fileName1, fileName2))
            {
                int file1byte = 0;
                int file2byte = 0;

                // Open a System.IO.FileStream for each file.
                // Note: With the 'using' keyword the streams 
                // are closed automatically.
                using (FileStream fileStream1 = new FileStream(fileName1, FileMode.Open),
                                  fileStream2 = new FileStream(fileName2, FileMode.Open))
                {
                    // Read and compare a byte from each file until a
                    // non-matching set of bytes is found or the end of
                    // file is reached.
                    do
                    {
                        file1byte = fileStream1.ReadByte();
                        file2byte = fileStream2.ReadByte();
                    }
                    while ((file1byte == file2byte) && (file1byte != -1));
                }

                return ((file1byte - file2byte) == 0);
            }
            else
            {
                return false;
            }
        }
    }
}
