﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Log
{
    public interface ILogWriter
    {
        void LogError(String message);
        void LogWarning(String message);
        void LogInfo(String message);
        void LogDebug(String message);
    }
}
