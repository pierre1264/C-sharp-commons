﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Log
{
    
    public static class LogManager
    {
        private static List<ILogWriter> _logWriters = new List<ILogWriter>();

        public static void LogError(String message)
        {
            foreach (ILogWriter writer in _logWriters)
            {
                writer.LogError("Error:\t" + message);
            }
        }

        public static void LogWarning(String message)
        {
            foreach (ILogWriter writer in _logWriters)
            {
                writer.LogWarning("Warning:\t" + message);
            }
        }

        public static void LogInfo(String message)
        {
            foreach (ILogWriter writer in _logWriters)
            {
                writer.LogInfo("Info:\t" + message);
            }
        }

        public static void LogDebug(String message)
        {
            foreach (ILogWriter writer in _logWriters)
            {
                writer.LogDebug("Debug:\t" + message);
            }
        }

        public static void AddListener(ILogWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (!_logWriters.Contains(writer))
            {
                _logWriters.Add(writer);
            }
        }

        public static void RemoveListener(ILogWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            _logWriters.Remove(writer);
        }
    }
}
