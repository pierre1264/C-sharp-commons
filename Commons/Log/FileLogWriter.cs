﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net;

namespace Commons.Log
{
    /// <summary>
    /// Add this to the assembly file [assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config")]
    /// Do not forget to use the log4Net configuration file
    /// </summary>
    public class FileLogWriter: ILogWriter
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FileLogWriter));

        public FileLogWriter()
        {        
        }

        public void LogDebug(string message)
        {
            log.Fatal(message);
        }

        public void LogError(string message)
        {
            log.Error(message);
        }

        public void LogInfo(string message)
        {
            log.Info(message);
        }

        public void LogWarning(string message)
        {
            log.Warn(message);
        }
    }
}
