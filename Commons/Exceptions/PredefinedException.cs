﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Exceptions
{
    public abstract class PredefinedException: Exception
    {
        public PredefinedException() : base()
        {
        }

        public PredefinedException(String message): base(message)
        {
        }

        public PredefinedException(String message, Exception exception): base(message, exception)
        {
        }
    }

    public class ElementAlreadyAddedException : PredefinedException{ }
    public class ElementDoesNotExistException : PredefinedException{ }
    public class OperationFailedException : PredefinedException { }

}
