﻿using Commons.Log;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Commons.Gui
{
    public partial class LogWindow : Form, ILogWriter
    {
        private static readonly Color ColorDebug = Color.DarkGray;
        private static readonly Color ColorInfo = Color.Black;
        private static readonly Color ColorWarning = Color.DarkOrange;
        private static readonly Color ColorError = Color.Red;

        public LogWindow()
        {
            InitializeComponent();
        }

        public void LogDebug(string message)
        {
            if (!String.IsNullOrWhiteSpace(input.Text))
            {
                input.AppendText("\n");
            }

            input.SelectionStart = input.TextLength;
            input.SelectionLength = 0;

            input.SelectionColor = ColorDebug;
            input.AppendText(message);
            input.SelectionColor = input.ForeColor;

            input.ScrollToCaret();
        }

        public void LogError(string message)
        {
            if (!String.IsNullOrWhiteSpace(input.Text))
            {
                input.AppendText("\n");
            }

            input.SelectionStart = input.TextLength;
            input.SelectionLength = 0;

            input.SelectionColor = ColorError;
            input.AppendText(message);
            input.SelectionColor = input.ForeColor;

            input.ScrollToCaret();
        }

        public void LogInfo(string message)
        {
            if (!String.IsNullOrWhiteSpace(input.Text))
            {
                input.AppendText("\n");
            }

            input.SelectionStart = input.TextLength;
            input.SelectionLength = 0;

            input.SelectionColor = ColorInfo;
            input.AppendText(message);
            input.SelectionColor = input.ForeColor;

            input.ScrollToCaret();
        }

        public void LogWarning(string message)
        {
            if (!String.IsNullOrWhiteSpace(input.Text))
            {
                input.AppendText("\n");
            }

            input.SelectionStart = input.TextLength;
            input.SelectionLength = 0;

            input.SelectionColor = ColorWarning;
            input.AppendText(message);
            input.SelectionColor = input.ForeColor;

            input.ScrollToCaret();
        }
    }
}
