﻿using System;
using System.IO;
using Commons.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Commons_test.Log
{
    [TestClass]
    public class LogFileWriterTest
    {
        public LogFileWriterTest()
        {
            LogManager.AddListener(new FileLogWriter());
        }

        [TestMethod]
        public void Test_LogFileWriter_WriteError()
        {
            String message = "testing log file writer " + DateTime.Now;
            LogManager.LogError(message);
            String lastLine = ReadLastLogLine();
            Assert.IsTrue(lastLine.Contains("Error:"));
            Assert.IsTrue(lastLine.Contains(message));
        }

        [TestMethod]
        public void Test_LogFileWriter_WriteWarning()
        {
            String message = "testing log file writer " + DateTime.Now;
            LogManager.LogWarning(message);
            String lastLine = ReadLastLogLine();
            Assert.IsTrue(lastLine.Contains("Warning:"));
            Assert.IsTrue(lastLine.Contains(message));
        }

        [TestMethod]
        public void Test_LogFileWriter_WriteInfo()
        {
            String message = "testing log file writer " + DateTime.Now;
            LogManager.LogInfo(message);
            String lastLine = ReadLastLogLine();
            Assert.IsTrue(lastLine.Contains("Info:"));
            Assert.IsTrue(lastLine.Contains(message));
        }

        [TestMethod]
        public void Test_LogFileWriter_WriteDebug()
        {
            String message = "testing log file writer " + DateTime.Now;
            LogManager.LogDebug(message);
            String lastLine = ReadLastLogLine();
            Assert.IsTrue(lastLine.Contains("Debug:"));
            Assert.IsTrue(lastLine.Contains(message));
        }

        private String ReadLastLogLine()
        {
            File.Copy("myapp.log", "myapp.log.unitTest", true);
            String[] lines = File.ReadAllLines("myapp.log.unitTest");
            return lines[lines.Length-1];
        }
    }
}
