﻿using Commons.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons_test.Log
{
    public class FakeLogger : ILogWriter
    {
        public const String NoLog = "NoLog";

        public String LastLog
        {
            get;
            private set;
        }

        public FakeLogger()
        {
            LastLog = NoLog;
        }

        public void LogDebug(string message)
        {
            LastLog = message;
        }

        public void LogError(string message)
        {
            LastLog = message;
        }

        public void LogInfo(string message)
        {
            LastLog = message;
        }

        public void LogWarning(string message)
        {
            LastLog = message;
        }
    }
}
