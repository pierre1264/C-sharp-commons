﻿using System;
using Commons.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Commons_test.Log
{
    [TestClass]
    public class LogManagerTest
    {
        public FakeLogger logger = new FakeLogger();

        [TestCleanup]
        public void RemoveLogger()
        {
            LogManager.RemoveListener(logger);
        }

        [TestMethod]
        public void LogError_EMessage_ErrorAndMessage()
        {
            LogManager.AddListener(logger);
            LogManager.LogError("an error");
            Assert.AreEqual("Error:\tan error", logger.LastLog);
        }

        [TestMethod]
        public void LogError_EMessage_NoLog()
        {
            LogManager.LogError("an error");
            Assert.AreEqual(FakeLogger.NoLog, logger.LastLog);
        }

        [TestMethod]
        public void LogWarning_WMessage_WarningAndMessage()
        {
            LogManager.AddListener(logger);
            LogManager.LogWarning("a warning");
            Assert.AreEqual("Warning:\ta warning", logger.LastLog);
        }

        [TestMethod]
        public void LogWarning_WMessage_NoLog()
        {
            LogManager.LogWarning("a warning");
            Assert.AreEqual(FakeLogger.NoLog, logger.LastLog);
        }

        [TestMethod]
        public void LogInfo_IMessage_InfoAndMessage()
        {
            LogManager.AddListener(logger);
            LogManager.LogInfo("an info");
            Assert.AreEqual("Info:\tan info", logger.LastLog);
        }

        [TestMethod]
        public void LogInfo_IMessage_NoLog()
        {
            LogManager.LogInfo("an info");
            Assert.AreEqual(FakeLogger.NoLog, logger.LastLog);
        }

        [TestMethod]
        public void LogDebug_DMessage_DebugAndMessage()
        {
            LogManager.AddListener(logger);
            LogManager.LogDebug("a message");
            Assert.AreEqual("Debug:\ta message", logger.LastLog);
        }

        [TestMethod]
        public void LogDebug_DMessage_NoLog()
        {
            LogManager.LogDebug("a message");
            Assert.AreEqual(FakeLogger.NoLog, logger.LastLog);
        }
    }
}
